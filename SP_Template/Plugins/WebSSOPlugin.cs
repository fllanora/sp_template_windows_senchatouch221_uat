﻿/*  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading;
using Microsoft.Devices.Sensors;
using System.Globalization;
using System.Diagnostics;

using WPCordovaClassLib.Cordova;
using WPCordovaClassLib.Cordova.Commands;
using WPCordovaClassLib.Cordova.JSON;
using System.Net;
using System.IO;
using System.Text;
using System.Net;




namespace WPCordovaClassLib.Cordova.Commands
{
 

public class WebSSOPlugin : BaseCommand
{

    private static string strUserID;
    private static string strPassword;
    private static string strCookie;
    private static string WebSSODomain = "";
    private static string postType = "";

    private static string webSSOCookie = "";

     public void setWebSSODomain(string options)
    {
       string[] optionsString = JSON.JsonHelper.Deserialize<string[]>(options);
       WebSSODomain = optionsString[0];
       System.Diagnostics.Debug.WriteLine("setWebSSODomain called: " + WebSSODomain);
    }

    public void formPost(string options)
    {
        // all JS callable plugin methods MUST have this signature!
        // public, returning void, 1 argument that is a string

      //  string[] args = JSON.JsonHelper.Deserialize<string[]>(options);
      //  string urlLoc = args[0];
       // string target = args[1];
        try
        {
        System.Diagnostics.Debug.WriteLine("startWebSSO called.");
       string url = "https://"+WebSSODomain+"/pkmslogin.form";
                //"https://mobileweb.sp.edu.sg/pkmslogin.form";
                //       string url = "https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";
                //  string url = "https://sso.sp.edu.sg/pkmslogin.form";
               //   string url = "http://www.fritzllanora.com";

       strUserID = "";
       strPassword = "";
       strCookie = "";
       postType = "";
       string[] optionsString = JSON.JsonHelper.Deserialize<string[]>(options);
 
//   try
   //    {

           strUserID = optionsString[1];
           strPassword = optionsString[2];
           strCookie = optionsString[3];
           postType = optionsString[4];
    //   }
    //   catch (Exception err)
      // {
    //   }

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

               // request.UseDefaultCredentials = true;
                request.AllowAutoRedirect = false;
              //  request.AllowAutoRedirect = true;
                if (postType == "credentialsLogin")
                {
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";

                    //   request.Method = "POST";
                    //    request.Method = "GET";
                    request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);  // for POST
                }
                else if (postType == "cookieLogin")
                {
                    request.Headers["Cookie"] = strCookie;//"PD-H-SESSION-ID=SZO5SmOBb7d6j/QZLSQrvxDOM/I9Y6RH84f9uJUVlgQDaGTlau0=_AAAAAAE=_ggzOS5IB9nqmA2AdqFrsDPQ9Zmo=;Domain=mobileweb.sp.edu.sg;Path=/";
                    request.BeginGetResponse(new AsyncCallback(GetRequestCompleted), request);  // for GET
                }
               
              //   request.BeginGetResponse(new AsyncCallback(GetRequestCompleted), request);  // for GET
            

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("inside Application_Launching() Exception: " + ex.Message);
            }

    }

    private void GetRequestCompleted(IAsyncResult asynchronousResult)
    {
        HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
      
        HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
        System.Diagnostics.Debug.WriteLine("Request Completed Code: " + (int)response.StatusCode);


        System.Diagnostics.Debug.WriteLine("response Headers: " + response.Headers);
        string locationResponseString = response.Headers["location"];
        System.Diagnostics.Debug.WriteLine("locationResponseString : " + locationResponseString);

       // Location


        using (StreamReader httpwebStreamReader = new StreamReader(response.GetResponseStream()))
        {
            string results = httpwebStreamReader.ReadToEnd();
            System.Diagnostics.Debug.WriteLine("results: " + results);
            DispatchCommandResult(new PluginResult(PluginResult.Status.OK, locationResponseString));
        
        }
    }

    private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
    {

        System.Diagnostics.Debug.WriteLine("inside GetRequestStreamCallback() : ");
        
        HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
        Stream postStream = request.EndGetRequestStream(asynchronousResult);

        string postCredentialsString = "username=" + strUserID + "&password=" + strPassword + "&login-form-type=pwd";

        string postData = string.Format(postCredentialsString);

        byte[] byteArray = Encoding.UTF8.GetBytes(postData);

        postStream.Write(byteArray, 0, postData.Length);
        postStream.Close();

        request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);

    }


    private void GetResponseCallback(IAsyncResult asynchronousResult)
    {

        try
        {

       //     Uri uri = new Uri("https://sso.sp.edu.sg/pkmslogin.form", UriKind.Absolute);

            System.Diagnostics.Debug.WriteLine("inside get response");


            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

         
           
            System.Diagnostics.Debug.WriteLine("inside get response: request created ");

          //  string accept_value = request.Accept;

         //   System.Diagnostics.Debug.WriteLine("inside get response: request accept_value: "+accept_value);
            System.Diagnostics.Debug.WriteLine("inside get response: request accept_value: ");

            // End the operation
            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
            System.Diagnostics.Debug.WriteLine("inside get response: response received ");


            Stream streamResponse = response.GetResponseStream();
            System.Diagnostics.Debug.WriteLine("inside get response: streamResponse ");


            System.Diagnostics.Debug.WriteLine("Inside GetResponseCallback : StatusDescription " + response.StatusDescription);
            System.Diagnostics.Debug.WriteLine("Inside GetResponseCallback : HttpStatusCode(int) " + (int)response.StatusCode);
            System.Diagnostics.Debug.WriteLine("Inside GetResponseCallback : HttpStatusCode " + response.StatusCode);

            StreamReader streamRead = new StreamReader(streamResponse);
            string responseString = streamRead.ReadToEnd();

            System.Diagnostics.Debug.WriteLine("response Headers: " + response.Headers);

            string cookieResponseString = response.Headers["Set-Cookie"];
            System.Diagnostics.Debug.WriteLine("cookieResponseString Test : " + cookieResponseString);
            //note: get the PD-H-SESSION-ID
            string locationResponseString = response.Headers["location"];
            System.Diagnostics.Debug.WriteLine("locationResponseString : " + locationResponseString);


            
            System.Diagnostics.Debug.WriteLine("cookieResponseString Appended : " + cookieResponseString);
            DispatchCommandResult(new PluginResult(PluginResult.Status.OK, cookieResponseString));
           // DispatchCommandResult(new PluginResult(PluginResult.Status.OK, "{result:\"super awesome!\"}"));
          

            streamResponse.Close();
            streamRead.Close();
            response.Close();


        }

        catch (WebException exc)
        {
            System.Diagnostics.Debug.WriteLine("WebException: " + exc.Message);
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine("Inside GetResponseCallback : Exception " + ex.Message);

        }

    }

    public void ajaxWebSSO(string options)
    {
        System.Diagnostics.Debug.WriteLine("ajaxWebSSO called");
        try
        {
     
           string[] optionsString = JSON.JsonHelper.Deserialize<string[]>(options);
           string url = optionsString[0];
           string httpCookie = optionsString[1];
           System.Diagnostics.Debug.WriteLine("ajaxWebSSO URL: " + url);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            // PD_STATEFUL_a1cde238-aba5-11e1-a074-00215ec8a2c8    mobileweb.sp.edu.sg-SSL
          // PD-H-SESSION-ID=modyZm2zm2fcKWirfnCY2ES8mGr/arnCBRsrWEZSySPV2knHU4Y=_AAAAAAE=_1zcDJH0drvDiSTeaPVgZXY0jZOg=
            request.Headers["Cookie"] = httpCookie;//"PD-H-SESSION-ID=SZO5SmOBb7d6j/QZLSQrvxDOM/I9Y6RH84f9uJUVlgQDaGTlau0=_AAAAAAE=_ggzOS5IB9nqmA2AdqFrsDPQ9Zmo=;Domain=mobileweb.sp.edu.sg;Path=/";
          //  request.Headers["Cookie"] = "PD-H-SESSION-ID=8CV0a2Lyu4Vmyg+lhTzfNkZ5ZeTvOEBTepNe4Fd1qc23/jbCquE=; Domain=.sp.edu.sg; Path=/";
          
            request.Method = "GET";
            request.BeginGetResponse(new AsyncCallback(GetAjaxWebssoRequestCompleted), request);

        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine("inside Application_Launching() Exception: " + ex.Message);
        }
 
    }

    private void GetAjaxWebssoRequestCompleted(IAsyncResult asynchronousResult)
    {

        try
        {
            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;


            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);

            System.Diagnostics.Debug.WriteLine("Request Completed Code: " + (int)response.StatusCode);


            System.Diagnostics.Debug.WriteLine("response Headers: " + response.Headers);

            using (StreamReader httpwebStreamReader = new StreamReader(response.GetResponseStream()))
            {
                string results = httpwebStreamReader.ReadToEnd();
                System.Diagnostics.Debug.WriteLine("results: " + results);
                DispatchCommandResult(new PluginResult(PluginResult.Status.OK, results));

            }
        }
        catch (Exception e)
        {
            DispatchCommandResult(new PluginResult(PluginResult.Status.OK, ""));
        }
    }


   

}
}