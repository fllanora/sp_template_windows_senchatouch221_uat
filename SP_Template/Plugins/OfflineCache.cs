﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using WPCordovaClassLib.Cordova;
using WPCordovaClassLib.Cordova.Commands;
using WPCordovaClassLib.Cordova.JSON;
using System.IO.IsolatedStorage;
using System.Net;

using System.Windows.Navigation;
using Windows.Phone.Storage.SharedAccess;
using System.Diagnostics;
using System.Reflection;
using System.Text.RegularExpressions;





namespace WPCordovaClassLib.Cordova.Commands
{
    class OfflineCache : BaseCommand
    {
        string fileName;
        public void downloadFile(string options)
        {

            string downloadUrlLink = "";
            string headerCookie = "";

            string[] optionsString = JSON.JsonHelper.Deserialize<string[]>(options);

              downloadUrlLink = optionsString[0];
              headerCookie = optionsString[2];
          
              System.Diagnostics.Debug.WriteLine("downloadUrlLink: " + downloadUrlLink);
              System.Diagnostics.Debug.WriteLine("headerCookie: " + headerCookie);


            System.Diagnostics.Debug.WriteLine("startOfflineDownload called: Native CS");


            downLoadProcess(downloadUrlLink, headerCookie);
         


        }



        void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {


            try
            {


                using (IsolatedStorageFile isoStore = IsolatedStorageFile.GetUserStoreForApplication())
                {

                   System.Diagnostics.Debug.WriteLine("File name is:  " + fileName);
                    using (IsolatedStorageFileStream isoStream = new IsolatedStorageFileStream(fileName, FileMode.Create, isoStore))                 
                    {
                        string filePathTest = isoStream.Name;
                        System.Diagnostics.Debug.WriteLine("filePathTests:$$$$$$$$$$$$$$ " + filePathTest);
                        System.Diagnostics.Debug.WriteLine("File created and Name is: " + fileName);
                        e.Result.CopyTo(isoStream);
                        isoStream.Close();
                    }


                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("wc_OpenReadCompleted Exception " + ex.Message);
            }
        }

        string GetFileName(String hrefLink)
        {
            string tmp = hrefLink;
            int LastIndex = tmp.LastIndexOf("/");
            tmp = tmp.Substring(LastIndex + 1, (tmp.Length - LastIndex - 1));

            System.Diagnostics.Debug.WriteLine("File name: " + tmp);

            fileName = tmp;
            return tmp;

        }

        void downLoadProcess(string weburl, string headerCookie)
        {

            System.Diagnostics.Debug.WriteLine("downLoadProcess() : weburl: " + weburl);

            string file_name_value = GetFileName(weburl);

            System.Diagnostics.Debug.WriteLine("downLoadProcess() : file_name_value: " + file_name_value);
            
            try
            {

                System.Diagnostics.Debug.WriteLine("inside try created");

              //  HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                // PD_STATEFUL_a1cde238-aba5-11e1-a074-00215ec8a2c8    mobileweb.sp.edu.sg-SSL
                // PD-H-SESSION-ID=modyZm2zm2fcKWirfnCY2ES8mGr/arnCBRsrWEZSySPV2knHU4Y=_AAAAAAE=_1zcDJH0drvDiSTeaPVgZXY0jZOg=
              //  request.Headers["Cookie"] = httpCookie;//"PD-H-SESSION-ID=SZO5SmOBb7d6j/QZLSQrvxDOM/I9Y6RH84f9uJUVlgQDaGTlau0=_AAAAAAE=_ggzOS5IB9nqmA2AdqFrsDPQ9Zmo=;Domain=mobileweb.sp.edu.sg;Path=/";
     

                WebClient wc = new WebClient();
                wc.OpenReadCompleted += new OpenReadCompletedEventHandler(wc_OpenReadCompleted);
                wc.OpenReadAsync(new Uri(weburl, UriKind.Absolute));
               // ebClient.Headers.Add(HttpRequestHeader.Cookie, "cookies");
              //  webClient.DownloadFile("http://........", "C://2.pdf");
                wc.Headers["Cookie"] = headerCookie;
                wc.UseDefaultCredentials = true;
              //  wc.Headers["Set-Cookie"] = headerCookie;
                

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception" + ex.Message);

            }


        }

    


        public void readOfflineCache(string options)
        {
            System.Diagnostics.Debug.WriteLine("Inside readOfflineCache:  called: ");
           
           string downloadUrlLink = "";

            string[] optionsString = JSON.JsonHelper.Deserialize<string[]>(options);

            downloadUrlLink = optionsString[0];

            string offlineFileContent = "";

            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            IsolatedStorageFileStream fileStream = myIsolatedStorage.OpenFile(downloadUrlLink, FileMode.Open, FileAccess.Read);
         

            using (StreamReader reader = new StreamReader(fileStream))
            {
                offlineFileContent = reader.ReadToEnd();
            }
            System.Diagnostics.Debug.WriteLine(">>>> offlineFileContent: " + offlineFileContent);
    
            DispatchCommandResult(new PluginResult(PluginResult.Status.OK, offlineFileContent));
        }

        public void requestNative(string options)
        {
            System.Diagnostics.Debug.WriteLine("*****************Inside responseXMLAlerts:  called: ");

            try
            {
                string requestCookie = "";


                string[] optionsString = JSON.JsonHelper.Deserialize<string[]>(options);
                string url = optionsString[0];//"https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";
              
                requestCookie = optionsString[1];




                System.Diagnostics.Debug.WriteLine("ajaxWebSSO URL: " + url);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                // PD_STATEFUL_a1cde238-aba5-11e1-a074-00215ec8a2c8    mobileweb.sp.edu.sg-SSL
                // PD-H-SESSION-ID=modyZm2zm2fcKWirfnCY2ES8mGr/arnCBRsrWEZSySPV2knHU4Y=_AAAAAAE=_1zcDJH0drvDiSTeaPVgZXY0jZOg=

                //request.Headers["Cookie"] = "PD-H-SESSION-ID=SZO5SmOBb7d6j/QZLSQrvxDOM/I9Y6RH84f9uJUVlgQDaGTlau0=_AAAAAAE=_ggzOS5IB9nqmA2AdqFrsDPQ9Zmo=;Domain=mobileweb.sp.edu.sg;Path=/";
                request.Headers["Cookie"] = requestCookie;


                //  request.Headers["Cookie"] = "PD-H-SESSION-ID=8CV0a2Lyu4Vmyg+lhTzfNkZ5ZeTvOEBTepNe4Fd1qc23/jbCquE=; Domain=.sp.edu.sg; Path=/";

                request.Method = "GET";
                request.BeginGetResponse(new AsyncCallback(GetRequestCompleted), request);
                
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("responseXMLAlerts() Exception: " + ex.Message);
            }

           
        }


       
        private void GetRequestCompleted(IAsyncResult asynchronousResult)
        {
            HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
            System.Diagnostics.Debug.WriteLine("Request Completed Code: " + (int)response.StatusCode);


            System.Diagnostics.Debug.WriteLine("response Headers: " + response.Headers);



             string results;
            using (StreamReader httpwebStreamReader = new StreamReader(response.GetResponseStream()))
            {
                results = httpwebStreamReader.ReadToEnd();
                System.Diagnostics.Debug.WriteLine("****************requestNative results: " + results);
                DispatchCommandResult(new PluginResult(PluginResult.Status.OK, results));
         

            }
          
        }


        public void getIsoRootPath(string options)
        {
            System.Diagnostics.Debug.WriteLine("Inside getIsoRootPath:  success");

            string fileName = "DeviceID.txt";
            string isolatedPath = "";

           // string[] optionsString = JSON.JsonHelper.Deserialize<string[]>(options);

           // fileName = optionsString[0];
          //  System.Diagnostics.Debug.WriteLine("getIsoRootPath() : fileName: " + fileName);

            // // C:/Data/Users/DefApps/AppData/{78AD1849-3753-42F0-81FB-6CBE74C3AAE1}/Local/
            try
            {
                System.Diagnostics.Debug.WriteLine("getIsoRootPath() : inside try: ");

                using (IsolatedStorageFile isoStore1 = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    System.Diagnostics.Debug.WriteLine("getIsoRootPath() : inside try: isoStore1 ");


                    IsolatedStorageFileStream oStream = new IsolatedStorageFileStream(fileName, FileMode.Open, isoStore1);
                    System.Diagnostics.Debug.WriteLine("getIsoRootPath() : inside try: oStream ");

                    isolatedPath = oStream.Name;

                    isolatedPath = isolatedPath.Substring(0, isolatedPath.Length - fileName.Length);
                    string isolatedPathVal = Regex.Replace(isolatedPath, @"\\", "/");
                    oStream.Close();

                    System.Diagnostics.Debug.WriteLine("Inside getIsoRootPath: isolatedPath:" + isolatedPathVal);



                    System.Diagnostics.Debug.WriteLine("Inside getIsoRootPath: isolatedPath.Replace " + isolatedPathVal);

                    //   isolatedPath = "C:/Data/Users/DefApps/AppData/{78AD1849-3753-42F0-81FB-6CBE74C3AAE1}/Local/";

                    DispatchCommandResult(new PluginResult(PluginResult.Status.OK, isolatedPathVal));
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Inside getIsoPath: Exception:" + ex.Message);

            }







        }



   
}

}

