Ext.define('app.store.myStore', {
    extend: 'Ext.data.Store',


    requires: [
        'app.model.myModel'
    ],

    config: {
        model: 'app.model.myModel',
        autoLoad: true,

        proxy: {
            type: 'ajax',
            url: 'data/GuestData.json',
            reader: {
                type: 'json',
                rootProperty: "guests"
            }
        },

        sorters: [{
            property : "guestName",
            direction: "ASC"
        }]

    }
});