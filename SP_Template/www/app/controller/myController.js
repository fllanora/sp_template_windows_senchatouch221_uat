Ext.define('app.controller.myController', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            MyList: 'myList',
            ListNavigationView: 'listNavigationView',
			logoutButton: 'listNavigationView titlebar button[action=logout]'

        },
        control: {
           'myList': {
                itemtap: 'onListItemTap'
            },
			
            'logoutButton': {
                tap: 'onLogoutTap'
            }
			
        }
    },
  
    onListItemTap: function(list, index, target, record) {
    
    this.getListNavigationView().push({
            title: record.data.guestName,
            xtype: 'myListDetails',
            record: record
        });
        
    },
	
	onLogoutTap: function(element, e) {
    
        try {
            PARAMS_DIRECTION = "none";
            PARAMS_STUDENT_ID = "";
			 SP.WebSSOPlugin.clearUserData();
        } 
        catch (err) {
        }
        
        var networkState = navigator.network.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.NONE] = 'No network connection';
        
        var checkConnection = states[networkState];
        
        if ((checkConnection == 'Unknown connection') ||
        (checkConnection == 'No network connection')) {
            if (PARAMS_STUDENT_ID == "") {
                Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                    navigator.app.exitApp();
                }).doComponentLayout();
                ;
                
            }
            else {
                if (OFFLINESUPPORT) {
                    Ext.Msg.alert("Offline Mode.", "You are offline. Login to view the previous data.").doComponentLayout();
                }
                else {
                    Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                        navigator.app.exitApp();
                    }).doComponentLayout();
                }
            }
            
        }
        else {
            try {
				var requestURL = "http://smobwebprd01.sf.sp.edu.sg/AlertWS/student/alert/RemoveDevice.jsp?" + PARAMS_DEVICE_TOKEN;
				
				Ext.Ajax.request({
					url: requestURL,
					method: "GET",
					success: function(response, opts){
							console.log("Device Token Successfully removed from server");
					},
					failure: function(){
						console.log("Failed removal of Device Token from server");
					}
					
				});
			}
			catch(err){};
        }
        
        	Ext.Viewport.removeAll(true,true);
			Ext.Viewport.add(Ext.create('app.view.WebSSOLoginView'));
    }
    

});
