/*
 This file is generated and updated by Sencha Cmd. You can edit this file as
 needed for your application, but these edits will have to be merged by
 Sencha Cmd when it performs code generation tasks such as generating new
 models, controllers or views and when running "sencha app upgrade".
 Ideally changes to this file would be limited and most work would be done
 in other places (such as Controllers). If Sencha Cmd cannot merge your
 changes and its generated code, it will produce a "merge conflict" that you
 will need to resolve manually.
 */
// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

var isLogEnabled = true;
var isDiagnosticEnabled = true;
var testStore;
var testList;
var segmentedButton;

var testCaseTimeout = 0;

//<debug>
Ext.Loader.setPath({
    'Ext': 'touch/src',
    'app': 'app'
});
//</debug>

Ext.application({
    name: 'app',
    
    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },
    
    isIconPrecomposed: true,
    
    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },
    
    launch: function(){
    
        
        Ext.define("Test", {
            extend: "Ext.data.Model",
            config: {
                fields: ['id', 'testType', 'testNumber', 'testDesc', 'status']
            }
        });
		
        
        testStore = Ext.create("Ext.data.Store", {
            model: "Test",
            sorters: 'id',
            id: 'testStore',
            name: 'testStore',
            grouper: {
				groupFn: function(record){
					return record.get('testType');
				}
			}
        });
        
        
        testList = Ext.create("Ext.List", {
			id: 'testList',
			grouped: true,
            store: testStore,
            styleHtmlCls: 'listClass',
            itemTpl: '<div class="test">{testNumber} - {id}<br>{testDesc}<br><b>{status}</b></div>',
            selModel: {
                mode: 'SINGLE'
            },
            styleHtmlContent: true,
                              listeners: {
                              itemtap: function(view, index, item, e){
                              /**
                               * Get the Store
                               */
                              var store = view.getStore();
                              var rec = store.getAt(index);
                              
                              var result = rec.get('testNumber');
                              
                              console.log(result);
                              
                              if ((result.indexOf("TC_LIBRARY_MP") != -1)) {
                              if ((result == "TC_LIBRARY_MP020") || (result == "TC_LIBRARY_MP021") || (result == "TC_LIBRARY_MP022") || (result == "TC_LIBRARY_MP023") || (result == "TC_LIBRARY_MP025") || (result == "TC_LIBRARY_MP050") || (result == "TC_LIBRARY_MP051") || (result == "TC_LIBRARY_MP060")) {
                              /**
                               * Show the Map Marker overlay
                               */
                              SP.OfflineCache.readCache('fritzllanoracomsencha2indexhtml.cache', function(result){
                                                        Ext.getCmp("cachePanel").update(result);
                                                        });
                              overlayMapMarker.setCentered(true);
                              overlayMapMarker.show();
                              Ext.Viewport.add(overlayMapMarker);
                              }
                              else
                              if ((result != "TC_LIBRARY_MP011") || (result == "TC_LIBRARY_MP010") || (result == "TC_LIBRARY_MP024") || (result == "TC_LIBRARY_MP030") || (result == "TC_LIBRARY_MP040") || (result == "TC_LIBRARY_MP041")) {
                              /**
                               * Show the Overlay
                               */
                              SP.OfflineCache.readCache('fritzllanoracomsencha2index2html.cache', function(result){
                                                        TC_LIBRARY_OC034();
                                                        Ext.getCmp("cacheManifestPanel").update(result);
                                                        });
                              overlay.setCentered(true);
                              overlay.show();
                              Ext.Viewport.add(overlay);
                              }
                              }
                              if ((result.indexOf("TC_LIBRARY_OC030") != -1)) {
                              /**
                               * Show the Cache Overlay panel
                               */
                              SP.OfflineCache.readCache('fritzllanoracomsencha2indexhtml.cache', function(result){
                                                        Ext.getCmp("cachePanel").update(result);
                                                        Ext.getCmp("cachePanel").doLayout();
                                                        Ext.Viewport.add(Ext.getCmp("testLandingPanel"));
                                                        });
                              
                              overlayCachePanel.setCentered(true);
                              overlayCachePanel.doLayout();
                              overlayCachePanel.show();
                              Ext.Viewport.add(overlayCachePanel);
                              }
                              if ((result.indexOf("TC_LIBRARY_OC034") != -1)) {
                              /**
                               * Show the SelectiveCache Overlay panel
                               */
                              SP.OfflineCache.readCache('fritzllanoracomsencha2index2html.cache', function(result){
                                                        Ext.getCmp("cacheManifestPanel").update(result);
                                                        Ext.getCmp("cacheManifestPanel").doLayout();
                                                        Ext.Viewport.add(Ext.getCmp("testLandingPanel"));
                                                        });
                              overlayCacheManifestPanel.setCentered(true);
                              overlayCacheManifestPanel.doLayout();
                              overlayCacheManifestPanel.show();
                              Ext.Viewport.add(overlayCacheManifestPanel);
                              }
                              if ((result.indexOf("TC_LIBRARY_WS033") != -1) || (result.indexOf("TC_LIBRARY_WS034") != -1)) {
                              /**
                               * Show the Testing Panel
                               */
                              try {
                              Ext.getCmp("testLandingPanel").setCentered(true);
                              Ext.getCmp("testLandingPanel").show();
                              Ext.Viewport.add(Ext.getCmp("testLandingPanel"));
                              }
                              catch (err) {
                              }
                              
                              }
                              if ((result.indexOf("TC_LIBRARY_WS035") != -1) || (result.indexOf("TC_LIBRARY_WS036") != -1)) {
                              try {
                              /**
                               * Display Login Form Panel
                               */
                              Ext.getCmp("testLoginFormPanel").setCentered(true);
                              Ext.getCmp("testLoginFormPanel").show();
                              Ext.Viewport.add(Ext.getCmp("testLoginFormPanel"));
                              }
                              catch (err) {
                              }
                              }
                              
                              }
                              }
        });
                
                /**
                 * Define the Overlay to be used in the test cases
                 */
                var overlay = Ext.create('Ext.Panel', {
                                            floating: true,
                                            modal: true,
                                            centered: false,
                                            width: 320,
                                            height: 380,
                                            styleHtmlContent: true,
                                            scroll: 'vertical',
                                            contentEl: 'map',
                                            cls: 'htmlcontent',
                                         hideAnimation: {
                                         type: 'popOut',
                                         duration: 200,
                                         easing: 'ease-out'
                                         },
                                         showAnimation: {
                                         type: 'popIn',
                                         duration: 200,
                                         easing: 'ease-out'
                                         },
                                         hideOnMaskTap: true
                                            });
                /**
                 * Define Map Marker overlay to be used in the test cases
                 */
                var overlayMapMarker = Ext.create('Ext.Panel', {
                                                     floating: true,
                                                     modal: true,
                                                     centered: false,
                                                     width: 320,
                                                     height: 380,
                                                     styleHtmlContent: true,
                                                     scroll: 'vertical',
                                                     contentEl: 'markerMap',
                                                     cls: 'htmlcontent',
                                                  hideAnimation: {
                                                  type: 'popOut',
                                                  duration: 200,
                                                  easing: 'ease-out'
                                                  },
                                                  showAnimation: {
                                                  type: 'popIn',
                                                  duration: 200,
                                                  easing: 'ease-out'
                                                  },
                                                  hideOnMaskTap: true
                                                     });
                /**
                 * Define Cache overlay panel
                 */
                var overlayCachePanel = Ext.create('Ext.Panel', {
                                                      id: 'cachePanel',
                                                      floating: true,
                                                      modal: true,
                                                      centered: false,
                                                      width: 320,
                                                      height: 380,
                                                      styleHtmlContent: true,
                                                      scroll: 'both',
                                                      cls: 'htmlcontent',
                                                   hideAnimation: {
                                                   type: 'popOut',
                                                   duration: 200,
                                                   easing: 'ease-out'
                                                   },
                                                   showAnimation: {
                                                   type: 'popIn',
                                                   duration: 200,
                                                   easing: 'ease-out'
                                                   },
                                                   hideOnMaskTap: true
                                                      });
                /**
                 * Define CacheManifest overlay panel
                 */
                var overlayCacheManifestPanel = Ext.create('Ext.Panel', {
                                                              id: 'cacheManifestPanel',
                                                              floating: true,
                                                              modal: true,
                                                              centered: false,
                                                              width: 320,
                                                              height: 380,
                                                              styleHtmlContent: true,
                                                              scroll: 'both',
                                                              cls: 'htmlcontent',
                                                           hideAnimation: {
                                                           type: 'popOut',
                                                           duration: 200,
                                                           easing: 'ease-out'
                                                           },
                                                           showAnimation: {
                                                           type: 'popIn',
                                                           duration: 200,
                                                           easing: 'ease-out'
                                                           },
                                                           hideOnMaskTap: true
                                                              });
      
        
		
		  /*******************************************************
         * Filters only the failed test cases
         *******************************************************/
        function filterFailedResult(){
            Ext.getCmp("testList").getStore().clearFilter(false);
            Ext.getCmp("testList").getStore().filter('status', '<font color="red">FAILED</font>');
            sortTestList();
            if (Ext.getCmp("testList").rendered) {
             /*   Ext.getCmp("testList").scroller.scrollTo({
                    x: 0,
                    y: 0
                });
                */
            }
        }
        /*******************************************************
         * Filters only the success test cases
         *******************************************************/
        function filterPassedResult(){
            Ext.getCmp("testList").getStore().clearFilter(false);
            Ext.getCmp("testList").getStore().filter('status', '<font color="green">PASSED</font>');
            sortTestList();
        }


       segmentedButton = Ext.create('Ext.SegmentedButton', {
            docked: 'bottom',
            id: 'bottomFilterButtons',
            layout: {
                type: 'hbox',
                pack: 'center',
                align: 'stretchmax'
            },
            allowMultiple: false,
            items: [{
                text: 'Passed',
                pressed: true,
                width: '50%',
                labelWidth: '100%',
                modal: true,
                handler: filterPassedResult
            }, {
                text: 'Failed',
                width: '50%',
                labelWidth: '100%',
                modal: true,
                handler: filterFailedResult
            }]
        });

		
 
        var testToolbar = Ext.create('Ext.Toolbar', {
            title: 'Test Cases',
            docked: 'top',
            items: [{
                xtype: 'spacer'
            }, {
                xtype: 'spacer'
            }, 
			{
                    iconCls: 'refresh',
                    iconMask: true,
						id: 'refreshBtn',
					  listeners: {
            

        initialize: function(){ 
		console.log("initialize refresh button ");
			if(device.platform=='Win32NT')
					   {
					  Ext.getCmp('refreshBtn').setIconCls('refresh win');
					   }
		
		
		}},

                handler: function(){
                    /**
                     * Clears the Store filter
                     */
                    try {
                        Ext.getCmp("testList").getStore().clearFilter(false);
                        sortTestList();
                    } 
                    catch (err) {
                    }
                    /** 
                     * Remove all data in the sotre and list
                     */
                    for (var i = Ext.getCmp("testList").getStore().getCount(); i > 0; i--) {
                        Ext.getCmp("testList").getStore().remove(Ext.getCmp("testList").getStore().last());
                    }
                    /** 
                     * Refresh the List
                     */
                    Ext.getCmp("testList").refresh();
					segmentedButton.setPressedButtons([0]);
					
                    /** 
                     * Calls the Library test Case function again
                     */
                    libraryTestFunction();
                }
				
            }]
        
        });

       var testCaseListPanel = Ext.create('Ext.Panel', {
            layout: 'fit',
            items: [testList, segmentedButton]
        });
		
	     var testCasePanel = Ext.create('Ext.Panel', {
         fullscreen: true,
         id: 'testCasePanel',
         layout: 'fit',
         items: [testToolbar,testCaseListPanel]
        });
		
		
    },
    
    onUpdated: function(){
        Ext.Msg.confirm("Application Update", "This application has just successfully been updated to the latest version. Reload now?", function(buttonId){
            if (buttonId === 'yes') {
                window.location.reload();
            }
        });
    }
});






/*******************************************************
 * Test Case Library Function
 *******************************************************/
function libraryTestFunction(){
    /** 
     * Display Loading Mask
     */
    Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Testing...'
        });
    
    /***********************************************/
    /***********************************************/
    /************** LIBRARY TEST CASES *************/
    /***********************************************/
    /***********************************************/
    
    
    /****************************************/
    /****** WEBSSO Library Test Cases *******/
    /****************************************/
    setTimeout("testWebSSOLibrary()", 1000 + testCaseTimeout);
    /****************************************/
    
    
    /****************************************/
    /******* MAP Library Test Cases *********/
    /****************************************/
    setTimeout("testMapLibrary()", 280000 + testCaseTimeout);
   //   setTimeout("testMapLibrary()", 1000); 
    /****************************************/
    
    /****************************************/
    /***** DATABASE Library Test Case *******/
    /****************************************/
    setTimeout("testDatabaseLibrary()", 310000 + testCaseTimeout);
   //    setTimeout("testDatabaseLibrary()",1000);
    /****************************************/
    
    
    /****************************************/
    /******  HTTP REQUEST Test Cases ********/
    /****************************************/
    setTimeout("testHttpRequestLibrary()", 130000 + testCaseTimeout);
   //  setTimeout("testHttpRequestLibrary()", 1000);
    /****************************************/
    
    
    /****************************************/
    /****** Offline Library Test Cases ******/
    /****************************************/
   setTimeout("testOfflineCacheLibrary()", 260000 + testCaseTimeout);
  //  setTimeout("testOfflineCacheLibrary()", 1000); 
    /****************************************/
    
    
    sortTestList();

//    setTimeout("sortTestList()", 10000);
  //  setTimeout("Ext.Viewport.setMasked(false);", 10000);
    try {
		/** 
	 * Filters the List when Passed Segmented button is selected
	 */
		if (segmentedButton.getPressed().getText() == 'Passed') {
			setTimeout("Ext.getCmp(\"testList\").getStore().filter('status', '<font color=\"green\">PASSED</font>')", 320000);
		}
		/** 
	 * Filters the List when Failed Segmented button is selected
	 */
		else 
			if (segmentedButton.getPressed().getText() == 'Failed') {
				setTimeout("Ext.getCmp(\"testList\").getStore().filter('status', '<font color=\"red\">FAILED</font>')", 320000);
			}
	}
	catch(err){}
    setTimeout("sortTestList()", 320000);
    setTimeout("Ext.Viewport.setMasked(false);", 320000);
    
    
}


/*******************************************************
 * Sort the Test List
 *******************************************************/
function sortTestList(){
    Ext.getCmp("testList").getStore().sort('id', 'ASC');
    Ext.getCmp("testList").refresh();
}

