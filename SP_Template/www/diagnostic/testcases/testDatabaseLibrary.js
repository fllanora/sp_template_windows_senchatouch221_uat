/******************************************************
 * Database Library Test Cases
 ******************************************************/

function testDatabaseLibrary()
{
	
    Ext.Viewport.setMasked({
        xtype: 'loadmask',
        message: 'Testing...'
    });
    
    /* Enable/Disable Large XML Download (16MB size, 9592 records, approx. 7-10min download) */
    var shouldDownloadLargeFile = false; 
    
    /******************************************************
     * TC_LIBRARY_DB010
     *
     * Create Database - Passing storeName and storeFields 
     *
     *****************************************************/
    var storeName = "testStore";
    var storeFields = ['id', 'name', 'gender'];
    SP.Database.createStore(storeName, storeFields);
    testStore.removeAll();
    SP.Database.addRecord(testStore, {
              id: 1,
              name: 'Ryan',
              gender: 'male'
              });
    if (SP.Database.getRecordCount(testStore) == 1)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10010,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB010',
                                              testDesc: ' Create Database - Passing storeName and storeFields',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10010,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB010',
                                              testDesc: ' Create Database - Passing storeName and storeFields',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB011
     *
     * Create Database - Passing storeName as Non String 
     *
     *****************************************************/
    var storeName = testStoreInvalid;
    var storeFields = ['id', 'name', 'gender'];
    SP.Database.createStore(storeName, storeFields);
    try{
        SP.Database.addRecord(testStoreInvalid, {
                  id: 1,
                  name: 'Ryan',
                  gender: 'male'
                  });
        if ((SP.Database.getRecordCount(testStoreInvalid) == "")||(SP.Database.getRecordCount(testStoreInvalid) == 0))
        {
            Ext.getCmp("testList").getStore().add({
                                                  id: 10011,
                                                  testType: 'Database',
                                                  testNumber: 'TC_LIBRARY_DB011',
                                                  testDesc: 'Create Database - Passing storeName as Non String',
                                                  status: '<font color="green">PASSED</font>'
                                                  });
        }
        else 
        {
            Ext.getCmp("testList").getStore().add({
                                                  id: 10011,
                                                  testType: 'Database',
                                                  testNumber: 'TC_LIBRARY_DB011',
                                                  testDesc: 'Create Database - Passing storeName as Non String',
                                                  status: '<font color="red">FAILED</font>'
                                                  });
        }
    }
    catch (err)
    {
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB012
     *
     * Create Database - Passing only storeName
     *
     *****************************************************/
    var storeName = "testStore2";
    var storeFields = ['id', 'name', 'gender'];
    SP.Database.createStore(storeName);
    try{
        SP.Database.addRecord(testStore2, {
                  id: 1,
                  name: 'Ryan',
                  gender: 'male'
                  });
        if ((SP.Database.getRecordCount(testStore2) == "")||(SP.Database.getRecordCount(testStore2) == 0))
        {
            Ext.getCmp("testList").getStore().add({
                                                  id: 10012,
                                                  testType: 'Database',
                                                  testNumber: 'TC_LIBRARY_DB012',
                                                  testDesc: 'Create Database - Passing only storeName',
                                                  status: '<font color="green">PASSED</font>'
                                                  });
        }
        else 
        {
            Ext.getCmp("testList").getStore().add({
                                                  id: 10012,
                                                  testType: 'Database',
                                                  testNumber: 'TC_LIBRARY_DB012',
                                                  testDesc: 'Create Database - Passing only storeName',
                                                  status: '<font color="red">FAILED</font>'
                                                  });
        }
    }
    catch (err)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10012,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB012',
                                              testDesc: 'Create Database - Passing only storeName',
                                              status: '<font color="green">PASSED</font>'
                                              });

    }
   
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB013
     *
     * Create Database - Passing databaseField as String
     *
     *****************************************************/
    var storeName = "testStore2";
    SP.Database.createStore(storeName, "storeFields");
    try{
        SP.Database.addRecord(testStore2, {
                  id: 1,
                  name: 'Ryan',
                  gender: 'male'
                  });
        if ((SP.Database.getRecordCount(testStore2) == "")||(SP.Database.getRecordCount(testStore2) == 0))
        {
            Ext.getCmp("testList").getStore().add({
                                                  id: 10013,
                                                  testType: 'Database',
                                                  testNumber: 'TC_LIBRARY_DB013',
                                                  testDesc: 'Create Database - Passing databaseField as String',
                                                  status: '<font color="green">PASSED</font>'
                                                  });
        }
        else 
        {
            Ext.getCmp("testList").getStore().add({
                                                  id: 10013,
                                                  testType: 'Database',
                                                  testNumber: 'TC_LIBRARY_DB013',
                                                  testDesc: 'Create Database - Passing databaseField as String',
                                                  status: '<font color="red">FAILED</font>'
                                                  });
        }
    }
    catch (err)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10013,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB013',
                                              testDesc: 'Create Database - Passing databaseField as String',
                                              status: '<font color="green">PASSED</font>'
                                              });
        
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB014
     *
     * Create Database - Passing storeFields with individual datatypes in Array 
     *
     *****************************************************/
    var storeName = "testStore3";
    var storeFields = [{
                          name: 'id',
                          type: 'int'
                          },
                          {
                          name: 'name',
                          type: 'string'
                          },
                          {
                          name: 'gender',
                          type: 'string'
                          }];
    SP.Database.createStore(storeName, storeFields);
    testStore3.removeAll();
    SP.Database.addRecord(testStore3, {
              id: 1,
              name: 'Ryan',
              gender: 'male'
              });
    if (SP.Database.getRecordCount(testStore3) == 1)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10014,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB014',
                                              testDesc: ' Create Database - Passing storeFields with individual datatypes in Array',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10014,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB014',
                                              testDesc: ' Create Database - Passing storeFields with individual datatypes in Array',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB015
     *
     * Create Database - Passing storeName and storeFields 
     *
     *****************************************************/
    var storeName = "testStore";
    var storeFields = ['id', 'name', 'gender'];
    SP.Database.createStore(storeName, storeFields);
    if (SP.Database.getRecordCount(testStore) == 0)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10015,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB015',
                                              testDesc: ' Create Database - Passing storeName which already exists in database',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10015,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB015',
                                              testDesc: ' Create Database - Passing storeName which already exists in database',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB020
     *
     * Add a new record - Passing valid inputs
     *
     *****************************************************/
    SP.Database.addRecord(testStore, {
              id: 2,
              name: 'Sandra',
              gender: 'female'
              });
    SP.Database.addRecord(testStore, {
              id: 3,
              name: 'Michael',
              gender: 'male'
              });
    var rec01 = SP.Database.findRecord(testStore, 'name', 'Sandra');
    var rec02 = SP.Database.findRecord(testStore, 'name', 'Michael');
    if ((SP.Database.getRecordCount(testStore) == 2) && (rec01.get('name') == 'Sandra') && (rec02.get('name') == 'Michael'))
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10020,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB020',
                                              testDesc: 'Add a new record - Passing valid inputs',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10020,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB020',
                                              testDesc: 'Add a new record - Passing valid inputs',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
       
    /******************************************************
     * TC_LIBRARY_DB021
     *
     * Add a new record - Passing string value instead of integer value in Array 
     *
     *****************************************************/
    SP.Database.addRecord(testStore3, {
              id: 2,
              name: 3,
              gender: 4
              });
    SP.Database.addRecord(testStore3, {
              id: 3,
              name: 3,
              gender: 4
              });
    if (SP.Database.getRecordCount(testStore3) == 3)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10021,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB021',
                                              testDesc: 'Add a new record - Passing string value instead of integer value in Array ',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10021,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB021',
                                              testDesc: 'Add a new record - Passing string value instead of integer value in Array ',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    testStore3.removeAll();
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB022
     *
     * Add a new record - Passing invalid table name 
     *
     *****************************************************/
    var testStoreInvalid;
    SP.Database.addRecord(testStoreInvalid, {
              id: 2,
              name: 'Sandra',
              gender: 'female'
              });
    if ((SP.Database.getRecordCount(testStoreInvalid) == 0) && (testStoreInvalid == null))
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10022,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB022',
                                              testDesc: 'Add a new record - Passing invalid table name',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10022,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB022',
                                              testDesc: 'Add a new record - Passing invalid table name',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/

    var rec;
    
    /******************************************************
     * TC_LIBRARY_DB030
     *
     * Sorts the Database - Ascending
     *
     *****************************************************/
    SP.Database.sortRecords(testStore, 'name', 'ASC');
    var rec = SP.Database.getFirstRecord(testStore);
    if (rec.get('name') == 'Michael')
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10030,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB030',
                                              testDesc: 'Sorts the Database - Ascending',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10030,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB030',
                                              testDesc: 'Sorts the Database - Ascending',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB031
     *
     * Sorts the Database - Descending
     *
     *****************************************************/
    SP.Database.sortRecords(testStore, 'name', 'DESC');
    rec = SP.Database.getLastRecord(testStore);
    if (rec.get('name') == 'Michael')
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10031,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB031',
                                              testDesc: 'Sorts the Database - Descending',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10031,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB031',
                                              testDesc: 'Sorts the Database - Descending',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB032
     *
     * Sorts the Database - Passing Invalid parameters
     *
     *****************************************************/
    var ASC = 123;
    SP.Database.sortRecords(testStore, 'name', ASC);
    if (rec.get('name') == 'Michael')
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10032,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB032',
                                              testDesc: 'Sorts the Database - Passing Invalid parameters',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else 
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10032,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB032',
                                              testDesc: 'Sorts the Database - Passing Invalid parameters',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }    
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB033
     *
     * Sorts the Database - Passing invalid table name
     *
     *****************************************************/
    var testStoreInvalid;
    SP.Database.sortRecords(testStoreInvalid, 'name', 'ASC');
    if ((SP.Database.getRecordCount(testStoreInvalid) == 0) && (testStoreInvalid == null))
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10033,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB033',
                                              testDesc: 'Sorts the Database - Passing invalid table name',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10033,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB033',
                                              testDesc: 'Sorts the Database - Passing Invalid parameters',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB040
     *
     * Find the particular record by fieldname and fieldvalue
     *****************************************************/
    rec = SP.Database.findRecord(testStore, 'name', 'Michael');
    if (rec.get('name') == 'Michael')
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10040,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB040',
                                              testDesc: 'Find the particular record by fieldname and fieldvalue',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10040,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB040',
                                              testDesc: 'Find the particular record by fieldname and fieldvalue',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB041
     *
     * Find the particular record by fieldname and fieldvalue - Passing invalid fieldname
     *****************************************************/
    var Michael = 123;
    rec = SP.Database.findRecord(testStore, 'name', Michael);
    try
    {
    if (rec.get('name') == 'Michael')
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10041,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB041',
                                              testDesc: 'Find the particular record by fieldname and fieldvalue - Passing invalid fieldname',
                                              status: '<font color="red">FAILED</font>'
                                              });
        
    }else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10041,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB041',
                                              testDesc: 'Find the particular record by fieldname and fieldvalue - Passing invalid fieldname',
                                              status: '<font color="green">PASSED</font>'
                                              });
 
    }
    }
    catch(err)
    {

        Ext.getCmp("testList").getStore().add({
                                              id: 10041,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB041',
                                              testDesc: 'Find the particular record by fieldname and fieldvalue - Passing invalid fieldname',
                                              status: '<font color="green">PASSED</font>'
                                              });

    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB050
     *
     * Filters the Database - Passing storeName, fieldName and fieldvalue
     *
     *****************************************************/
    SP.Database.filterRecords(testStore, 'gender', 'male');
    if (SP.Database.getRecordCount(testStore) == 0)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10050,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB050',
                                              testDesc: 'Filters the Database - Passing storeName, fieldName and fieldvalue',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10050,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB050',
                                              testDesc: 'Filters the Database - Passing storeName, fieldName and fieldvalue',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    testStore.clearFilter(true);
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB051
     *
     * Filters the Database - Passing Invalid Parameters
     *
     *****************************************************/
    var male = 123;
    SP.Database.filterRecords(testStore, 'gender', male);
    if (SP.Database.getRecordCount(testStore) == 3)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10051,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB051',
                                              testDesc: 'Filters the Database - Passing Invalid Parameters',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else if (SP.Database.getRecordCount(testStore) == 1)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10051,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB051',
                                              testDesc: 'Filters the Database - Passing Invalid Parameters',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }

    testStore.clearFilter(true);
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB060
     *
     * Delete the particular record
     *****************************************************/
    rec = SP.Database.findRecord(testStore, 'name', 'Ryan');
    SP.Database.deleteRecord(testStore, rec)
    if (testStore.getCount() == 2)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10060,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB060',
                                              testDesc: 'Delete the particular record',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10060,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB060',
                                              testDesc: 'Delete the particular record',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB061
     *
     * Delete the particular record - invalid table name
     *****************************************************/
    var testStoreInvalid;
    rec = SP.Database.findRecord(testStoreInvalid, 'name', 'Ryan');
    SP.Database.deleteRecord(testStoreInvalid, rec)
    if ((SP.Database.getRecordCount(testStoreInvalid) == 0) && (testStoreInvalid == null) && (testStore.getCount() == 2))
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10061,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB061',
                                              testDesc: 'Delete the particular record - invalid table name',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10061,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB061',
                                              testDesc: 'Delete the particular record - invalid table name',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
       
    /******************************************************
     * TC_LIBRARY_DB070
     *
     * Get the first record in the database
     *
     *****************************************************/
    SP.Database.sortRecords(testStore, 'name', 'ASC');
    var rec = SP.Database.getFirstRecord(testStore);
    if (rec.get('name') == 'Michael')
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10070,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB070',
                                              testDesc: 'Get the first record in the database',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10070,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB070',
                                              testDesc: 'Get the first record in the database',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB071
     *
     * Get the first record in the database - Passing Table name which doesn't exist in database
     *
     *****************************************************/
    var testStoreWrong;
    //doesn't exist in database
    SP.Database.sortRecords(testStoreWrong, 'name', 'ASC');
    var rec = SP.Database.getFirstRecord(testStoreWrong);
    if (rec == "")
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10071,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB071',
                                              testDesc: 'Get the first record in the database - Passing Table name which doesn\'t exist in database',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10071,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB071',
                                              testDesc: 'Get the first record in the database - Passing Table name which doesn\'t exist in database',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB080
     *
     * Get the last record in the database
     *
     *****************************************************/
    SP.Database.sortRecords(testStore, 'name', 'DESC');
    rec = SP.Database.getLastRecord(testStore);
    if (rec.get('name') == 'Michael')
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10080,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB080',
                                              testDesc: 'Get the last record in the database',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10080,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB080',
                                              testDesc: 'Get the last record in the database',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    SP.Database.sortRecords(testStore, 'id', 'ASC');
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB081
     *
     * Get the last record in the database - Passing Table name which doesn't exist in database
     *
     *****************************************************/
    var testStoreWrong;
    //doesn't exist in database
    SP.Database.sortRecords(testStoreWrong, 'name', 'ASC');
    var rec = SP.Database.getLastRecord(testStoreWrong);
    if (rec == "")
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10081,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB081',
                                              testDesc: 'Get the last record in the database - Passing Table name which doesn\'t exist in database',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10081,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB081',
                                              testDesc: 'Get the last record in the database - Passing Table name which doesn\'t exist in database',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB090
     *
     * Get the total number of records in the database
     *
     *****************************************************/
    var tableCount = SP.Database.getRecordCount(testStore);
    if (tableCount == 2)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10090,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_D090',
                                              testDesc: 'Get the total number of records in the database',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10090,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB090',
                                              testDesc: 'Get the total number of records in the database',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB100
     *
     * Update existing record
     *
     *****************************************************/
    rec = SP.Database.findRecord(testStore, 'name', 'Michael');
    SP.Database.updateRecord(testStore, rec, 'name', 'Steven')
    if (rec.get('name') == 'Steven')
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10100,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_D190',
                                              testDesc: 'Update Record - Passing storeName, record, fieldName and fieldValue',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10100,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB100',
                                              testDesc: 'Update Record - Passing storeName, record, fieldName and fieldValue',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    
    /******************************************************
     * TC_LIBRARY_DB110
     *
     * Clear all the data of the database
     *
     *****************************************************/
    SP.Database.clearStore(testStore);
    if (testStore.getCount() == 0)
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10110,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB110',
                                              testDesc: 'Clear all the data of the database',
                                              status: '<font color="green">PASSED</font>'
                                              });
    }
    else
    {
        Ext.getCmp("testList").getStore().add({
                                              id: 10110,
                                              testType: 'Database',
                                              testNumber: 'TC_LIBRARY_DB110',
                                              testDesc: 'Clear all the data of the database',
                                              status: '<font color="red">FAILED</font>'
                                              });
    }
    /**/
    SP.Database.clearStore(testStore);
    SP.Database.clearStore(testStore3);
    
    /******************************************************
     * TC_LIBRARY_DB120 
     *
     * Downloading Large XMl file and load to data store (16MB size, 9592 records)
     *
     *****************************************************/
     if(shouldDownloadLargeFile)
     {
     setTimeout("TC_LIBRARY_DB120()", 80000);       
     }
}

/******************************************************
 * TC_LIBRARY_DB120
 *
 * Downloading Large XMl file and load to data store (16MB size, 9592 records)
 *
 *****************************************************/
function TC_LIBRARY_DB120()
{
    /* notes: 
     XML size = 16MB size
     No. of Records =  9592 records
     Average Downloading Time = 7-10min
     */
    var xmlUrl = 'http://www.fritzllanora.com/sp/testLargeXML.xml'; // (large xml)
  //   var xmlUrl = 'http://www.fritzllanora.com/sp/Alerts3.xml'; //(small xml, 6 records)
    var xmlNumRecords = 0;
    Ext.regModel('LargeAlerts', {
                 fields: [
                          {name: 'AlertType', type: 'string'},
                          {name: 'AlertName',  type: 'string'},
                          {name: 'AlertDescription',  type: 'string'},
                          {name: 'AlertSummary',  type: 'string'}
                          ]
                 });
    /******************************************************
     * Define XML Large Store
     *****************************************************/
    var xmlLargeStore = new Ext.data.Store({
                                           model: 'LargeAlerts',
                                           method: 'GET',
                                           autoLoad: true,
                                           proxy: {
                                           type: 'ajax',
                                           url : xmlUrl,
                                           reader: {
                                           type  : 'xml',
                                           root  : 'Alerts',
                                           record: 'Alert'
                                           }
                                           }
                                           });
    Ext.getBody().mask('Testing... ', 'x-mask-loading', false);
    /******************************************************
     * Triggers after the all data are loaded in XML Large Store
     *****************************************************/    
    xmlLargeStore.on('load', function(){
                     xmlNumRecords = xmlLargeStore.getCount();   
                     var xmlRecordsString = "("+ xmlNumRecords+" RECORDS)";    
                     if(xmlNumRecords > 0 )
                     {
                     Ext.getCmp("testList").getStore().add({
                                                           id: 10120,
                                                           testType: 'Database',
                                                           testNumber: 'TC_LIBRARY_DB120',
                                                           testDesc: 'Downloading Large XML file and load to data store '+xmlRecordsString,
                                                           status: '<font color="green">PASSED</font>'
                                                           });
                     }
                     else
                     {
                     Ext.getCmp("testList").getStore().add({
                                                           id: 10120,
                                                           testType: 'Database',
                                                           testNumber: 'TC_LIBRARY_DB120',
                                                           testDesc: 'Downloading Large XML file and load to data store '+xmlRecordsString,
                                                           status: '<font color="red">FAILED</font>'
                                                           });
                     }
                     Ext.getCmp("testList").getStore().sort('id', 'ASC');
                     Ext.getCmp("testList").refresh();
                     Ext.getCmp("testList").getStore().filter('status', '<font color=\"green\">PASSED</font>');
                     Ext.getBody().unmask();
                     });
}
/**/