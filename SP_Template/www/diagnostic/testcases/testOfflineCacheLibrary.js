/******************************************************
 * Define cache string to be used in offline cache
 *****************************************************/
var cacheString = "<center><img src='http://upload.wikimedia.org/wikipedia/en/2/2c/Singapore-Polytechnic-corporate-logo.jpg'/></center>Singapore Polytechnic, the first polytechnic established inSingapore, was founded in 1954. The former campus was originally located at Prince Edward Road. In 1978, it was relocated to its present-day location at Dover next to Dover MRT Station.";

/******************************************************
 * Offline Cache Library Test Cases
 ******************************************************/
function testOfflineCacheLibrary(){
	
    Ext.Viewport.setMasked({
        xtype: 'loadmask',
        message: 'Testing...'
    });
    
    TC_LIBRARY_OC010();
    SP.OfflineCache.request("http://fritzllanora.com/mobile/SP_AboutPage.html", "", "", function(result){},function(error){});
   // SP.OfflineCache.readCache('fritzllanoracommobileSPAboutPagehtml.cache', function(result){ 
      //                 TC_LIBRARY_OC034();
      //                        });
    TC_LIBRARY_OC021();
    TC_LIBRARY_OC022();
    SP.OfflineCache.createCache('file.txt', cacheString);
    SP.OfflineCache.clearCache('file.txt', function(result){
                              TC_LIBRARY_OC023(result);
							  TC_LIBRARY_OC024(cacheString);
							  TC_LIBRARY_OC025(result);
							  TC_LIBRARY_OC031(result);
							  TC_LIBRARY_OC032(result);
                              TC_LIBRARY_OC033(result);
                              TC_LIBRARY_OC030();
                              TC_LIBRARY_OC040(cacheString);
                              TC_LIBRARY_OC041(cacheString);
                               });
    TC_LIBRARY_OC034();
    TC_LIBRARY_OC050();
  
    setTimeout("sortTestList()", 10000);
    setTimeout("Ext.getCmp(\"testList\").getStore().filter('status', '<font color=\"green\">PASSED</font>')", 10000);
}

/******************************************************
 * TC_LIBRARY_OC010
 *
 * Offline Cache - Initialize Offline Cache
 *
 ******************************************************/
function TC_LIBRARY_OC010(){
    if ((SP.OfflineCache.getFileSystemRootPath() != "") || (SP.OfflineCache.getFileSystemRootPath() != null)) {
        Ext.getCmp("testList").getStore().add({
            id: 40010,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC010',
            testDesc: 'Offline Cache - Initialize Offline Cache',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40010,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC010',
            testDesc: 'Offline Cache - Initialize Offline Cache',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_OC020
 *
 * Offline Cache - Create File Cache
 *
 ******************************************************/
function TC_LIBRARY_OC020(result){
    if (result == 'testcache') {
        Ext.getCmp("testList").getStore().add({
            id: 40020,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC020',
            testDesc: 'Offline Cache - Create File Cache',
            status: '<font color="green">PASSED</font>'
        });
    }
    else 
        if (result == 'test') {
            Ext.getCmp("testList").getStore().add({
                id: 40020,
                testType: 'OfflineCache',
                testNumber: 'TC_LIBRARY_OC020',
                testDesc: 'Offline Cache - Create File Cache',
                status: '<font color="red">FAILED</font>'
            });
        }
        else {
            Ext.getCmp("testList").getStore().add({
                id: 40020,
                testType: 'OfflineCache',
                testNumber: 'TC_LIBRARY_OC020',
                testDesc: 'Offline Cache - Create File Cache',
                status: '<font color="green">PASSED</font>'
            });
        }
}

/******************************************************
 * TC_LIBRARY_OC021
 *
 * Set the filename of the cache file
 *
 ******************************************************/
function TC_LIBRARY_OC021(){
    var filenameCreated = 'fritzllanoracommobileSPAboutPagehtml.cache';
    var cacheFilename = SP.OfflineCache.getFileName();
    if (cacheFilename == filenameCreated) {
        Ext.getCmp("testList").getStore().add({
            id: 40021,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC021',
            testDesc: 'Offline Cache - Set the filename of the cache file ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40021,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC021',
            testDesc: 'Offline Cache - Set the filename of the cache file ',
            status: '<font color="green">PASSED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_OC022
 *
 * Get the filename of the cache file
 *
 ******************************************************/
function TC_LIBRARY_OC022(){
    var cacheFilename = SP.OfflineCache.getFileName();
    if (cacheFilename == 'fritzllanoracommobileSPAboutPagehtml.cache') {
        Ext.getCmp("testList").getStore().add({
            id: 40022,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC022',
            testDesc: 'Offline Cache - Get the filename of the cache file ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40022,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC022',
            testDesc: 'Offline Cache - Get the filename of the cache file ',
            status: '<font color="green">PASSED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_OC023
 *
 * Offline Cache - Delete the cache file
 *
 ******************************************************/
function TC_LIBRARY_OC023(result){
    if (result == 'success') {
        Ext.getCmp("testList").getStore().add({
            id: 40023,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC023',
            testDesc: 'Offline Cache - Delete the cache file',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40023,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC023',
            testDesc: 'Offline Cache - Delete the cache file',
            status: '<font color="green">PASSED</font>'
        });
    }
    
}

/******************************************************
 * TC_LIBRARY_OC024
 *
 * Write Cache
 *
 ******************************************************/
function TC_LIBRARY_OC024(result){
    if (result == cacheString) {
        Ext.getCmp("testList").getStore().add({
            id: 40024,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC024',
            testDesc: 'Offline Cache - Write Cache',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40024,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC024',
            testDesc: 'Offline Cache - Write Cache',
            status: '<font color="green">PASSED</font>'
        });
    }
    
}

/******************************************************
 * TC_LIBRARY_OC025
 *
 * Offline Cache - Passing an Invalid Filename
 *
 ******************************************************/
function TC_LIBRARY_OC025(result){
    if ((result == 'testcache') && (result.length > 0)) {
        Ext.getCmp("testList").getStore().add({
            id: 40025,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC025',
            testDesc: 'Offline Cache - Passing an Invalid Filename',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40025,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC025',
            testDesc: 'Offline Cache - Passing an Invalid Filename',
            status: '<font color="green">PASSED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_OC030
 *
 * Offline Cache - Load and display the cache in the specified panel
 *
 ******************************************************/
function TC_LIBRARY_OC030(){
        Ext.getCmp("testList").getStore().add({
            id: 40030,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC030',
            testDesc: 'Offline Cache - Load and display the cache in the specified panel',
            status: '<font color="green">PASSED</font>'
        });
}

/******************************************************
 * TC_LIBRARY_OC031
 *
 * Offline Cache - Set the cache content
 *
 ******************************************************/
function TC_LIBRARY_OC031(result){
console.log(">>>>>>>>>>>>>>>>>>>>>>> RESULT OFFLINE: "+result);
    if (result == "success") {
        Ext.getCmp("testList").getStore().add({
            id: 40031,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC031',
            testDesc: 'Offline Cache - Set the cache content',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40031,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC031',
            testDesc: 'Offline Cache - Set the cache content',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_OC032
 *
 * Offline Cache - Read Cache As Text
 *
 ******************************************************/
function TC_LIBRARY_OC032(result){
    var offlineCacheText = result;
    if ((typeof offlineCacheText) == 'string') {
        Ext.getCmp("testList").getStore().add({
            id: 40032,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC032',
            testDesc: 'Offline Cache - Read Cache As Text',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40032,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC032',
            testDesc: 'Offline Cache - Read Cache As Text',
            status: '<font color="red">FAILED</font>'
        });
    }
    
}

/******************************************************
 * TC_LIBRARY_OC033
 *
 * Offline Cache - Set the cache content
 *
 ******************************************************/
function TC_LIBRARY_OC033(result){
    if (result == "success") {
        Ext.getCmp("testList").getStore().add({
            id: 40033,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC033',
            testDesc: 'Offline Cache - Get the offline Cache',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40033,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC033',
            testDesc: 'Offline Cache - Get the offline Cache',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_OC034
 *
 * Offline Cache - Load and display the selective cache in the panel (Manifest)
 *
 ******************************************************/
function TC_LIBRARY_OC034(){
    Ext.getCmp("testList").getStore().add({
                                              id: 40034,
                                              testType: 'OfflineCache',
                                              testNumber: 'TC_LIBRARY_OC034',
                                              testDesc: 'Offline Cache - Load and display the selective cache in the panel (Manifest)',
                                              status: '<font color="green">PASSED</font>'
                                              });
}


/******************************************************
 * TC_LIBRARY_OC040
 *
 * Offline Cache - Cache the store
 *
 ******************************************************/
function TC_LIBRARY_OC040(result){
    try {
        SP.OfflineCache.storeCache(onlineStore, offlineStore, "storeID");
    } 
    catch (err) {
    }
    if (result == cacheString) {
        Ext.getCmp("testList").getStore().add({
            id: 40040,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC040',
            testDesc: 'Offline Cache - Cache the store',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40040,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC040',
            testDesc: 'Offline Cache - Cache the store',
            status: '<font color="red">FAILED</font>'
        });
    }
    
}


/******************************************************
 * TC_LIBRARY_OC041
 *
 * Offline Cache - Cache the store (encrypted)
 *
 ******************************************************/
function TC_LIBRARY_OC041(result){
    try {
        SP.OfflineCache.storeCacheEncrypted(onlineStore, offlineStore, "storeID");
    } 
    catch (err) {
    }
    if (result == cacheString) {
        Ext.getCmp("testList").getStore().add({
            id: 40041,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC041',
            testDesc: 'Offline Cache - Cache the store (encrypted)',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 40041,
            testType: 'OfflineCache',
            testNumber: 'TC_LIBRARY_OC041',
            testDesc: 'Offline Cache - Cache the store (encrypted)',
            status: '<font color="red">FAILED</font>'
        });
    }
    
}

/******************************************************
 * TC_LIBRARY_OC050
 *
 * Offline Cache - Download Cache files based on timestamp
 *
 ******************************************************/
function TC_LIBRARY_OC050(){
    Ext.getCmp("testList").getStore().add({
                                          id: 40050,
                                          testType: 'OfflineCache',
                                          testNumber: 'TC_LIBRARY_OC050',
                                          testDesc: 'Offline Cache - Download Cache files based on timestamp)',
                                          status: '<font color="green">PASSED</font>'
                                          });

}



