/*******************************************************
 * Map Library Test Cases
 *******************************************************/
function testMapLibrary(){
	
    Ext.Viewport.setMasked({
        xtype: 'loadmask',
        message: 'Testing...'
    });

    setTimeout("locationMapTest()", 1000 + testCaseTimeout);
    setTimeout("markerMapTest()", 10000 + testCaseTimeout);
    setTimeout("wrongMapTest()", 20000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP010()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP011()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP020()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP021()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP022()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP023()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP024()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP025()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP030()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP040()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP041()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP050()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP051()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_MP060()", 22000 + testCaseTimeout);
    setTimeout("sortTestList()", 23000 + testCaseTimeout);
    setTimeout("Ext.getCmp(\"testList\").getStore().filter('status', '<font color=\"green\">PASSED</font>')", 23000 + testCaseTimeout);
}

/*******************************************************
 * TC_LIBRARY_MP010
 *
 * Map - Map Initialization - Valid MAP_SERVER
 *
 *******************************************************/
function TC_LIBRARY_MP010(){
    console.log(">>>>> START TEST MAP");
    if (status_LIBRARY_MP010) {
        Ext.getCmp("testList").getStore().add({
            id: 20010,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP010',
            testDesc: 'Map - Map Initialization - Valid MAP_SERVER',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20010,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP010',
            testDesc: 'Map - Map Initialization - Valid MAP_SERVER',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP011
 *
 * Map - Map Initialization - Invalid MAP_SERVER
 *
 *******************************************************/
function TC_LIBRARY_MP011(){
    if (status_LIBRARY_MP011) {
        Ext.getCmp("testList").getStore().add({
            id: 20011,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP011',
            testDesc: 'Map - Map Initialization - Invalid MAP_SERVER',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20011,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP011',
            testDesc: 'Map - Map Initialization - Invalid MAP_SERVER',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP020
 *
 * Add marker to the map
 *
 *******************************************************/
function TC_LIBRARY_MP020(){
    if (status_LIBRARY_MP020) {
        Ext.getCmp("testList").getStore().add({
            id: 20020,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP020',
            testDesc: 'Add marker to the map ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20020,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP020',
            testDesc: 'Add marker to the map ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP021
 *
 * Add Marker - Invalid image source
 *
 *******************************************************/
function TC_LIBRARY_MP021(){
    if (status_LIBRARY_MP021) {
        Ext.getCmp("testList").getStore().add({
            id: 20021,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP021',
            testDesc: 'Add Marker - Invalid image source ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20021,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP021',
            testDesc: 'Add Marker - Invalid image source ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP022
 *
 * Add Marker - image parameter missing
 *
 *******************************************************/
function TC_LIBRARY_MP022(){
    if (status_LIBRARY_MP022) {
        Ext.getCmp("testList").getStore().add({
            id: 20022,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP022',
            testDesc: 'Add Marker - image parameter missing ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20022,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP022',
            testDesc: 'Add Marker - image parameter missing ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP023
 *
 * Add Marker - passing empty string as markerName
 *
 *******************************************************/
function TC_LIBRARY_MP023(){
    if (status_LIBRARY_MP023) {
        Ext.getCmp("testList").getStore().add({
            id: 20023,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP023',
            testDesc: 'Add Marker - passing empty string as markerName ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20023,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP023',
            testDesc: 'Add Marker - passing empty string as markerName ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP024
 *
 * Map - Change Image of location markers
 *
 *******************************************************/
function TC_LIBRARY_MP024(){
    if (status_LIBRARY_MP024) {
        Ext.getCmp("testList").getStore().add({
            id: 20024,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP024',
            testDesc: 'Map - Change Image of location markers ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20024,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP024',
            testDesc: 'Map - Change Image of location markers ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP025
 *
 * Add Marker - passing NULL locations
 *
 *******************************************************/
function TC_LIBRARY_MP025(){
    if (status_LIBRARY_MP025) {
        Ext.getCmp("testList").getStore().add({
            id: 20025,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP025',
            testDesc: 'Add Marker - passing NULL locations ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20025,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP025',
            testDesc: 'Add Marker - passing NULL locations ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP030
 *
 * Map - Zoom To Location
 *
 *******************************************************/
function TC_LIBRARY_MP030(){
    if (status_LIBRARY_MP030) {
        Ext.getCmp("testList").getStore().add({
            id: 20030,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP030',
            testDesc: 'Map - Zoom To Location ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20030,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP030',
            testDesc: 'Map - Zoom To Location ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/*******************************************************
 * TC_LIBRARY_MP040
 *
 * Map - Show Location
 *
 *******************************************************/
function TC_LIBRARY_MP040(){
    if (status_LIBRARY_MP040) {
        Ext.getCmp("testList").getStore().add({
            id: 20040,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP040',
            testDesc: 'Map - Show Location ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20040,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP040',
            testDesc: 'Map - Show Location ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/********************************************************
 * TC_LIBRARY_MP041
 *
 * Map - Get current location, accuracy, region proximity.
 *
 ********************************************************/
function TC_LIBRARY_MP041(){
    if (status_LIBRARY_MP041) {
        Ext.getCmp("testList").getStore().add({
            id: 20041,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP041',
            testDesc: 'Map - Get current location, accuracy, region proximity. ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20041,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP041',
            testDesc: 'Map - Get current location, accuracy, region proximity. ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/********************************************************
 * TC_LIBRARY_MP050
 *
 * Map - Draw Route from one Pin to another
 *
 ********************************************************/
function TC_LIBRARY_MP050(){
    if (status_LIBRARY_MP050) {
        Ext.getCmp("testList").getStore().add({
            id: 20050,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP050',
            testDesc: 'Map - Draw Route from one Pin to another ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20050,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP050',
            testDesc: 'Map - Draw Route from one Pin to another ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/********************************************************
 * TC_LIBRARY_MP051
 *
 * Map - Set Color of Route path
 *
 ********************************************************/
function TC_LIBRARY_MP051(){
    if (status_LIBRARY_MP051) {
        Ext.getCmp("testList").getStore().add({
            id: 20051,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP051',
            testDesc: 'Map - Set Color of Route path ',
            status: '<font color="green">PASSED</font>'
        });
        
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20051,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP051',
            testDesc: 'Map - Set Color of Route path ',
            status: '<font color="red">FAILED</font>'
        });
    }
}

/********************************************************
 * TC_LIBRARY_MP060
 *
 * Display details of Call Out Info Window
 *
 ********************************************************/
function TC_LIBRARY_MP060(){
    if (status_LIBRARY_MP060) {
        Ext.getCmp("testList").getStore().add({
            id: 20060,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP060',
            testDesc: 'Display details of Call Out Info Window ',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 20060,
            testType: 'Map',
            testNumber: 'TC_LIBRARY_MP060',
            testDesc: 'Display details of Call Out Info Window ',
            status: '<font color="red">FAILED</font>'
        });
    }
}



/********************************************************
 * Location Map Test
 *
 ********************************************************/
function locationMapTest(){
    LOCATION_MARKER_GRAPHIC = "diagnostic/images/bluedot.png";
    DIV_MAP_NAME = "map";
    LOCATION_ZOOM_LEVEL = 17;
    SP.Map.addMaptoPage();
}

/********************************************************
 * Marker Map Test
 *
 ********************************************************/
function markerMapTest(){
    DIV_MAP_NAME = "markerMap";
    LOCATION_ZOOM_LEVEL = 11;
    SP.Map.addMaptoPage();
}

/********************************************************
 * Wrong Map Test
 *
 ********************************************************/
function wrongMapTest(){
    DIV_MAP_NAME = "wrongMap";
    SP.Map.addMaptoPage();
}

