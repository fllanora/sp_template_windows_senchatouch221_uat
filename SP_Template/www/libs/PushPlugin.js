/*****************************************************
 * Push Notification Library
 *
 * Functions:  
 *      PushPlugin.registerDevice: function(types, success, fail)
 *      PushPlugin.registerToken(deviceToken)
 *
 *****************************************************/
var SP = SP || {};

SP.PushPlugin = {
    /*****************************************************
     * Method for Registering Device for Push Notification
     * @param {Array} types 
     * @param {Function callback} success Success callback
     * @param {Function callback} fail Fail callback
     *
     *****************************************************/
registerDevice: function(types, success, fail) {

     return cordova.exec(success, fail, "PushPlugin", "registerDevice", types);
   // return PhoneGap.exec(success, fail, "PushPlugin", "registerDevice", types);
}, /* end of registerDevice */
    
    /*****************************************************
     * Method for Registering Device Token to Push Server
     * @param {String} deviceToken Device Token 
     *
     *****************************************************/    
registerToken: function(deviceToken, deviceOS) {
  
    
	var networkState = navigator.connection.type;
            var states = {};
            states[Connection.UNKNOWN] = 'Unknown connection';
            states[Connection.ETHERNET] = 'Ethernet connection';
            states[Connection.WIFI] = 'WiFi connection';
            states[Connection.CELL_2G] = 'Cell 2G connection';
            states[Connection.CELL_3G] = 'Cell 3G connection';
            states[Connection.CELL_4G] = 'Cell 4G connection';
            states[Connection.CELL] = 'Cell generic connection';
            states[Connection.NONE] = 'No network connection';
            var checkConnection = states[networkState];
                            if ((checkConnection == 'Unknown connection') ||
							(checkConnection == 'No network connection')) {
							
							}
							else {
								var requestURL = "https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertPost.jsp?" + deviceToken + "&" + deviceOS;
								//var requestURL = "https://mobileweb-tst.testsf.testsp.edu.sg/AlertWS/student/alert/AlertPost.jsp?" + deviceToken + "&" + deviceOS;
								
								console.log("register token: " + deviceToken + " ---- " + deviceOS);
								
								
								Ext.Ajax.request({
									url: requestURL,
									method: "GET",
									success: function(){
										console.log("Push Token Registration success");
										
									},
									failure: function(){
										console.log("Push Token Registration failed");
										
									}
									
								});
							}

},/* end of registerDevice */
    
getMessageAlert: function(types, success, fail) {
    
    return cordova.exec(success, fail, "PushPlugin", "getMessageAlert", types);
    // return PhoneGap.exec(success, fail, "PushPlugin", "registerDevice", types);
}, /* end of getMessageId */    

getMessageId: function(types, success, fail) {
    
    return cordova.exec(success, fail, "PushPlugin", "getMessageId", types);
    // return PhoneGap.exec(success, fail, "PushPlugin", "registerDevice", types);
}, /* end of getMessageId */   

setMessageId: function(types, success, fail) {
    //  alert("test");
    return cordova.exec(success, fail, "PushPlugin", "setMessageId", types);
    // return PhoneGap.exec(success, fail, "PushPlugin", "registerDevice", types);
} /* end of setMessageId */    	
	

	
};




