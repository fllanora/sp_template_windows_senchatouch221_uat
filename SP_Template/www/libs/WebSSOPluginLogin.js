/*****************************************************
 * WebSSO Library
 *
 * LOGIN PAGE
 *
 *****************************************************/
function WebSSOloginPage(){
    if (isLogEnabled) {
        console.log("LOG: WebSSOloginPage()");
    }

	  try {
         //   app.views.viewport.removeAll();
        } 
        catch (err) {
        }
     	var networkState = navigator.connection.type;
																	var states = {};
																	states[Connection.UNKNOWN] = 'Unknown connection';
																	states[Connection.ETHERNET] = 'Ethernet connection';
																	states[Connection.WIFI] = 'WiFi connection';
																	states[Connection.CELL_2G] = 'Cell 2G connection';
																	states[Connection.CELL_3G] = 'Cell 3G connection';
																	states[Connection.CELL_4G] = 'Cell 4G connection';
																	states[Connection.CELL] = 'Cell generic connection';
																	states[Connection.NONE] = 'No network connection';
																	var checkConnection = states[networkState];
																	if ((checkConnection == 'Unknown connection')
																			|| (checkConnection == 'No network connection')) {
                               if(device.platform=='Win32NT')
							   {
							   PARAMS_STUDENT_ID = "p122995";
							   }


            if (PARAMS_STUDENT_ID == "") {
                Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                    navigator.app.exitApp();
                }).doComponentLayout();
                ;
            }
            else {
                if (OFFLINESUPPORT) {
					//alert("werts");
                   // Ext.Msg.alert("Offline Mode.", "You are offline. Login to view the previous data.").doComponentLayout();

                    Ext.Viewport.add(Ext.create('app.view.WebSSOLoginView'));
					Ext.Msg.alert("Offline Mode.", "You are offline. Login to view the previous data.").doComponentLayout();

					//Ext.Msg.alert("Offline Mode.", "You are offline. Login to view the previous data.");
                  
				}
                else {
                    Ext.Msg.alert("Connection Required", "This app requires an active internet connection to operate. 3G and Wi-Fi are supported. Please connect to network and restart the app.", function(){
                        navigator.app.exitApp();
                    }).doComponentLayout();
                }
            }
            
        }
		Ext.Viewport.removeAll(true,true);
		Ext.Viewport.add(Ext.create('app.view.WebSSOLoginView'));
		
}
